#include "disk.h"


void save(){
    const std::array<int, 8>& arr = read();
    std::ofstream file("data.txt");
    if (file.is_open()){
        for (auto i : arr){
            file << i << std::endl;
        }
        file.close();
    }
    else {
        std::cout << "File not exist\n";
    }
}

void load(){
    std::array<int, 8> arr;
    std::ifstream file("data.txt");
    if (file.is_open()){
        for (auto& i : arr){
            file >> i;
        }
        file.close();
    }
    else {
        std::cout << "File not exist\n";
    }
    write(arr);
}