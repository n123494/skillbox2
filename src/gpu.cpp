#include "gpu.h"

void display(){
    const std::array<int, 8>& arr = read();
    for (auto i : arr){
        std::cout << i << std::endl;
    }
}

void display (const int& sum){
    std::cout << "Sum: " << sum << std::endl;
}