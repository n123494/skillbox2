#include "ram.h"


std::array<int, 8> nums;

std::array<int, 8>& read(){
    return nums;
}

void write(std::array<int, 8> arr){
    nums = std::move(arr);
}

void write(){
    for (auto& i : nums){
        std::cout << "Input number: ";
        i = stoi(kbdInput());
    }
}