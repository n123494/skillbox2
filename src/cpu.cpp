#include "cpu.h"

int compute(){
    const std::array<int, 8>& arr = read();
    int sum = 0;
    for (auto i : arr){
        sum += i;
    }
    return sum;
}