#include "ram.h"
#include "gpu.h"
#include "cpu.h"
#include "disk.h"
#include "kbd.h"
#include <iostream>
#include <string>

int main(int argc, char **argv)
{    
    std::string input;
	while (true){
        std::cout << "Enter command or 'exit': ";
        input = kbdInput();
        if (input == "exit"){
            break;
        }
        if (input == "sum"){
            display(compute());
        }
        if (input == "save"){
            save();
        }
        if (input == "load"){
            load();
        }
        if (input == "input"){
            write();
        }
        if (input == "display"){
            display();
        }
    }
	return 0;
}
