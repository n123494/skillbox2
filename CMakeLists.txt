cmake_minimum_required(VERSION 3.23.0 FATAL_ERROR)

# Project name
set(PROJECT_NAME Skillbox2)
project(${PROJECT_NAME})

# This setting is useful for providing JSON file used by CodeLite for code completion
set(CMAKE_EXPORT_COMPILE_COMMANDS 1)

set(CONFIGURATION_NAME "Debug")

add_subdirectory(src)

